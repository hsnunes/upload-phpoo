<?php ?>
<!doctype html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap Form Upload</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
        <h2 class="my-5 d-flex justify_contents_between">
          Formulário Upload Imagem
          <a class="btn" href="/list">Lista</a>
        </h2>
        <form action="/save" method="post" enctype="multipart/form-data">
        <?=$inputHiddenId?>
        <div class="mb-3">
        <label for="exampleInputImagem" class="form-label">Imagem</label>
        <input name="imagem" type="file" accept="image/*" class="form-control" id="exampleInputImagem" aria-describedby="imagemHelp">
        <div id="imagemHelp" class="form-text <?=$dnone ?>"><?=$imagem ?? '' ?> - Imagem Atual</div>
        </div>
        <div class="mb-3">
        <label for="exampleInputLink" class="form-label">Link</label>
        <input name="link" value="<?= $link ?? '' ?>" type="text" class="form-control" id="exampleInputLink">
        </div>
        <div class="mb-3">
        <label for="exampleInputBotao" class="form-label">Botão</label>
        <input name="botao" value="<?= $botao ?? '' ?>" type="text" class="form-control" id="exampleInputBotao">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
  </body>
</html>