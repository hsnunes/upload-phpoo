<?php

$link = filter_input(INPUT_POST, 'link', FILTER_DEFAULT);
if ($link === false) {
    header('Location: /form');
    return;
}
$botao = filter_input(INPUT_POST, 'botao');
if ($botao === false) {
    header('Location: /form');
    return;
}

$img = null;
$id = null;
if ( $_FILES['imagem']['error'] === UPLOAD_ERR_OK )
{
    move_uploaded_file(
        $_FILES['imagem']['tmp_name'],
        __DIR__ . '/img/uploads/' . $_FILES['imagem']['name']
    );
    $img = $_FILES['imagem']['name'];

    $sql = 'INSERT INTO carousel (imagem, link, botao) VALUES (:imagem, :link, :botao)';
    if ( array_key_exists('id', $_POST) )
    {
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT );
        $sql = 'UPDATE carousel SET imagem = :imagem, link = :link, botao = :botao WHERE id = :id';
    }

    $statement = $pdo->prepare($sql);
    if ( $id !== null ) {
        $statement->bindValue(':id',  $id);
    }
    $statement->bindValue(':imagem',  $img);
    $statement->bindValue(':link', $link );
    $statement->bindValue(':botao', $botao);

    $result = $statement->execute();
    header('Location: /list');
} else {
    header('Location: /form');
}