<?php

declare(strict_types=1);

require_once __DIR__ . '/../vendor/autoload.php';


$dbPath = __DIR__ . '/../banco.sqlite';
$pdo = new PDO("sqlite:$dbPath");

$requestURL = parse_url( $_SERVER['REQUEST_URI'] );
$uri = $requestURL['path'];

if ( $_SERVER['REQUEST_METHOD'] === 'GET' )
{
    if ( $uri === '/list' ) {
        $imgs = $pdo->query("SELECT * FROM carousel");
        $rows = $imgs->fetchAll();
        require_once __DIR__ . '/lista.php';
    }
    if ( $uri === '/form' )
    {
        $inputHiddenId = '';
        $dnone = 'd-none';
        if ( array_key_exists('edit', $_GET) )
        {
            $id = filter_input(INPUT_GET, 'edit', FILTER_VALIDATE_INT);
    
            $stmt = $pdo->prepare('SELECT * FROM carousel WHERE id = :edit');
            $stmt->bindParam(':edit', $id);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_ASSOC);
            extract($res);

            $inputHiddenId = "<input name='id' type='hidden' value='{$id}'>";
            $dnone = '';

        }
        require_once __DIR__ . '/form.php';
    }

    if ( $uri === '/save' && array_key_exists('delete', $_GET) )
    {
        $id = filter_input(INPUT_GET, 'delete', FILTER_VALIDATE_INT);

        $stmt = $pdo->prepare('DELETE FROM carousel WHERE id = :delete');
        $stmt->bindParam(':delete', $id);
        $res = $stmt->execute();
        header('Location: /list?delete=ok');
        return;
    }
}

if ( $_SERVER['REQUEST_METHOD'] === 'POST' )
{
    require_once __DIR__ . '/save.php';
}


