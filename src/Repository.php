<?php

namespace HsNunes\labsPhp;

use PDO;
use Entity;
use stdClass;

class Repository
{

    public PDO $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Undocumented function
     *
     * @param Entity $ent
     * @return boolean
     */
    public function add(stdClass $ent): bool
        {
            $sql = 'INSERT INTO carousel (imagem, link, botao) VALUES (?, ?, ?)';
            $statement = $this->pdo->prepare($sql);
            $statement->bindValue(1, $ent->imagem);
            $statement->bindValue(2, $ent->link);
            $statement->bindValue(3, $ent->botao);

            return $statement->execute();

        }
}
